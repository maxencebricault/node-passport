const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require('jsonwebtoken')

const User = require('../models/Users')

const SECRET_KEY = process.env.SECRET || 'SECRET';

router.post("/register_login", (req, res, next) => {
    passport.authenticate("local", function(err, user, info) {
        if (err) {
            return res.status(500).json({ errors: err });
        }
        if (!user) { // Check why no user found...
            return res.status(500).json({ errors: "No user found" });
        }
        req.logIn(user, function(err) {
            if (err) {
                return res.status(500).json({ errors: err });
            }
            // Generate JWT
            const token = jwt.sign({id: user.id}, SECRET_KEY, null, null);
            return res.status(200).json({ success: token });
        });
    })(req, res, next);
});

router.get('/profile', (req, res, next) => {
    // Récuperer le token
    let token;

    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        token =  req.headers.authorization.split(' ')[1];
    } else if (req.query && req.query.token) {
        token = req.query.token;
    }
     const decoded = jwt.verify(token, SECRET_KEY, null, null);

    // Récuperer l'utilisateur associé
    User.findOne({_id: decoded.id}).then(user => {
        if (user) {
            res.json(user.email);
        } else {
            res.send('Utilisateur non trouvé')
        }
    });

    // Renvoyer son email
});


module.exports = router;
