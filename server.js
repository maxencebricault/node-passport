const express = require("express");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');

const passport = require("./passport/setup");
const auth = require("./routes/auth");

const app = express();
const PORT = process.env.PORT || 6000;
const MONGO_URI = process.env.MONGO_URI || "mongodb://127.0.0.1:27017/passport_login";


mongoose
  .connect(MONGO_URI, {useNewUrlParser: true})
  .then(console.log(`MongoDB connected ${MONGO_URI}`))
  .catch(err => console.log(err));

// Express Session
app.use(
  session({
    secret: "very secret this is",
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({mongooseConnection: mongoose.connection})
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Bodyparser middleware, extended false does not allow nested payloads
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Routes
app.use("/api/auth", auth);
app.get("/", (req, res) => res.send("Good morning sunshine!"));

app.listen(PORT, () => console.log(`Backend listening on port ${PORT}!`));
